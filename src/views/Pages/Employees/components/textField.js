import React from 'react';
import PropTypes from 'prop-types';
import { fieldInputPropTypes, fieldMetaPropTypes } from 'redux-form';

import {
  FormGroup, Label, Input,
} from 'reactstrap';


const textField = (field) => {
  const {
    input: { name, ...restInput },
    meta: { touched, error },
    type,
    label,
    ...custom
  } = field;
  return (
    <FormGroup>
      {label && (
        <Label htmlFor={name}>{label}</Label>
      )}
      <Input
        type={type}
        name={name}
        {...restInput}
        {...custom}
      />
      {touched && error && (
        <span className="help-block text-danger">{error}</span>
      )}
    </FormGroup>
  );
};

textField.propTypes = {
  input: PropTypes.shape(fieldInputPropTypes).isRequired,
  meta: PropTypes.shape(fieldMetaPropTypes).isRequired,
  label: PropTypes.string,
  type: PropTypes.oneOf(['text', 'email', 'phone']),
};


textField.defaultProps = {
  label: '',
  type: 'text',
};

export default textField;
