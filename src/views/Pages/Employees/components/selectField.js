import React from 'react';
import PropTypes from 'prop-types';
import { fieldInputPropTypes, fieldMetaPropTypes } from 'redux-form';

import {
  FormGroup, Label,
} from 'reactstrap';


const selectField = (
  {
    input: { name, ...restInput },
    label,
    selectText,
    options,
    meta: { touched, error },
    ...custom
  }
) => {
  const renderSelectOptions = (key, index) => (
    <option
      key={`${index}-${key}`}
      value={key}
    >
      {options[key]}
    </option>
  );

  if (options) {
    return (
      <FormGroup>
        {label && (
          <Label htmlFor={name}>{label}</Label>
        )}
        <select name={name} className="form-control" {...restInput} {...custom}>
          <option value="">{selectText}</option>
          {Object.keys(options).map(renderSelectOptions)}
        </select>
        {touched && error && (
          <span className="help-block text-danger">{error}</span>
        )}
      </FormGroup>
    );
  }
  return <div />;
};

function objectWithNumericKeys(obj) {
  if (Object.keys(obj).some(key => isNaN(key))) { return new Error('Validation failed!'); }
  return true;
}

selectField.propTypes = {
  input: PropTypes.shape(fieldInputPropTypes).isRequired,
  meta: PropTypes.shape(fieldMetaPropTypes).isRequired,
  label: PropTypes.string,
  selectText: PropTypes.string,
  options: PropTypes.objectOf(objectWithNumericKeys),
};


selectField.defaultProps = {
  label: '',
  selectText: 'Select',
  options: {},
};

export default selectField;
