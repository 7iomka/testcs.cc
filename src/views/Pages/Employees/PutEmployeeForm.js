import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import {
  Card, CardFooter, CardBlock, Form,
} from 'reactstrap';

const NewEmployeeForm = (props) => {
  const {
    handleSubmit,
    onSubmitHandler,
    pristine,
    reset,
    submitting,
    renderTextField,
    renderSelectField,
    departments,
  } = props;

  const availableDepartmentsOptions = departments.reduce((prev, curr) => {
    const { id, name } = curr;
    const currObj = { [id]: name };
    return {
      ...prev,
      ...currObj,
    };
  }, {});

  return (
    <Form action="" method="post" onSubmit={handleSubmit(onSubmitHandler)} >
      <Card>
        <CardBlock className="card-body">
          <Field
            name="firstName"
            component={renderTextField}
            label="First Name"
          />
          <Field
            name="lastName"
            component={renderTextField}
            label="Last Name"
          />
          <Field
            name="departmentId"
            component={renderSelectField}
            label="Department"
            options={availableDepartmentsOptions}
          />
        </CardBlock>
        <CardFooter>
          <button type="submit" className="btn btn-success"><i className="fa fa-dot-circle-o" /> Submit</button>
          <button type="reset" className="btn btn-secondary" onClick={reset} disabled={pristine || submitting} ><i className="fa fa-ban" /> Reset</button>
        </CardFooter>
      </Card>
    </Form>
  );
};

NewEmployeeForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmitHandler: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  departments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  renderTextField: PropTypes.func.isRequired,
  renderSelectField: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
};
NewEmployeeForm.defaultProps = {

};

export default NewEmployeeForm;
