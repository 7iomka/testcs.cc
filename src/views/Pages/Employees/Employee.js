import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Badge, Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';


class Employee extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // state for danger modal before we delete the employee
      danger: false,
    };

    this.toggleUpdateModal = this.toggleUpdateModal.bind(this);
    this.removeEmployee = this.removeEmployee.bind(this);
    this.toggleDanger = this.toggleDanger.bind(this);
  }

  toggleUpdateModal() {
    const {
      id,
      firstName,
      lastName,
      departmentId,
      onChangeCurrent,
      onToggleModal,
    } = this.props;
    onChangeCurrent({
      id,
      firstName,
      lastName,
      departmentId,
    });
    onToggleModal('update');
  }

  removeEmployee() {
    const { id } = this.props;
    this.props.onRemoveEmployee({ id });
  }

  toggleDanger() {
    this.setState({
      danger: !this.state.danger,
    });
  }


  render() {
    const {
      id,
      firstName,
      lastName,
      departmentId,
      departmentName,
    } = this.props;
    return (
      <tr>
        <td>{firstName}</td>
        <td>{lastName}</td>
        <td>{departmentName} (id: {departmentId})</td>
        <td>
          <Badge color="warning">{id}</Badge>{' '}
          <Button onClick={this.toggleUpdateModal} className="mr-2" color="success" >Edit Employee</Button>
          <Button color="danger" onClick={this.toggleDanger} ><i className="icon-trash" /></Button>
          {
            this.state.danger && (
              <Modal
                isOpen={this.state.danger}
                toggle={this.toggleDanger}
                className="modal-danger"
              >
                <ModalHeader toggle={this.toggleDanger}>Are you sure?</ModalHeader>
                <ModalBody>
                  Are you sure you want to remove the employee?
                </ModalBody>
                <ModalFooter>
                  <Button color="danger" onClick={this.removeEmployee}>Yes, remove it!</Button>{' '}
                  <Button color="secondary" onClick={this.toggleDanger}>Cancel</Button>
                </ModalFooter>
              </Modal>
            )
          }
        </td>
      </tr>

    );
  }
}
Employee.propTypes = {
  id: PropTypes.number.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  departmentId: PropTypes.number.isRequired,
  departmentName: PropTypes.string.isRequired,
  onChangeCurrent: PropTypes.func.isRequired,
  onToggleModal: PropTypes.func.isRequired,
  onRemoveEmployee: PropTypes.func.isRequired,
};

Employee.defaultProps = {

};

export default Employee;
