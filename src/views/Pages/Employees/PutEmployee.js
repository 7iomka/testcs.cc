import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  // formValueSelector,
} from 'redux-form';
import {
  Modal, ModalHeader, ModalBody,
} from 'reactstrap';

import renderTextField from './components/textField';
import renderSelectField from './components/selectField';

import {
  getDepartments,
} from '../../../redux/actions/departments';

import {
  NEW_EMPLOYEE_FORM_NAME,
} from '../../../redux/actions/employees';

import PutEmployeeForm from './PutEmployeeForm';

const validate = (values) => {
  const errors = {};
  const requiredFields = [
    'firstName',
    'lastName',
    'departmentId',
  ];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });

  return errors;
};

class PutEmployee extends Component {
  constructor(props) {
    super(props);

    this.addEmployee = this.addEmployee.bind(this);
    this.updateEmployee = this.updateEmployee.bind(this);
  }


  addEmployee(data) {
    const {
      onAddEmployee,
      reset,
      toggleModal,
    } = this.props;
    onAddEmployee({
      ...data,
      departmentId: Number(data.departmentId),
    });
    reset();
    toggleModal();
  }

  updateEmployee(data) {
    const {
      onUpdateEmployee,
      reset,
      toggleModal,
    } = this.props;

    onUpdateEmployee({
      ...data,
      departmentId: Number(data.departmentId),
    });

    reset();
    toggleModal();
  }


  render() {
    const {
      goal,
      isOpenModal,
      onToggleModal,
    } = this.props;
    return (
      <Modal
        isOpen={isOpenModal}
        toggle={onToggleModal}
        className="modal-success"
      >
        <ModalHeader toggle={onToggleModal}>{goal === 'create' ? 'Create a new' : 'Edit'} employee</ModalHeader>
        <ModalBody>
          <PutEmployeeForm
            {...this.props}
            renderTextField={renderTextField}
            renderSelectField={renderSelectField}
            onSubmitHandler={goal === 'create' ? this.addEmployee : this.updateEmployee}
          />
        </ModalBody>
      </Modal>
    );
  }
}

PutEmployee.propTypes = {
  goal: PropTypes.string.isRequired,
  departments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  isOpenModal: PropTypes.bool.isRequired,
  onToggleModal: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
  onAddEmployee: PropTypes.func.isRequired,
  onUpdateEmployee: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
};

// const selector = formValueSelector(NEW_EMPLOYEE_FORM_NAME);

const mapStateToProps = (state, ownProps) => {
  // firstName: selector(state, 'firstName'),
  // lastName: selector(state, 'lastName'),
  // departmentId: Number(selector(state, 'departmentId')),
  const { goal, initData } = ownProps;
  return {
    initialValues: goal === 'update' ? initData : null,
    departments: state.departments,
  };
};


const mapDispatchToProps = dispatch =>
  bindActionCreators({
    getDepartments,
  }, dispatch);


// Decorate with reduxForm(). It will read the initialValues prop provided by connect()
const formed = reduxForm({
  form: NEW_EMPLOYEE_FORM_NAME, // a unique identifier for this form
  destroyOnUnmount: false, // <------ preserve form data
  enableReinitialize: true,
  validate,
})(PutEmployee);

// You have to connect() to any reducers that you wish to connect to yourself
const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(formed);

export default connected;
