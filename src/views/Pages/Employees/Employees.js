import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  Row, Col,
  Card, CardBlock, CardHeader,
  Table, Button,
} from 'reactstrap';

import {
  getEmployees,
  addEmployee,
  updateEmployee,
  removeEmployee,
} from '../../../redux/actions/employees';

import {
  getDepartments,
} from '../../../redux/actions/departments';

import Employee from './Employee';
import PutEmployee from './PutEmployee';

class Employees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenModal: false,
      goal: 'create',
      currentEmployee: {},
    };
    this.changeGoal = this.changeGoal.bind(this);
    this.changeCurrent = this.changeCurrent.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleCreateModal = this.toggleCreateModal.bind(this);
  }

  componentWillMount() {
    if (!this.props.departments.length) {
      this.props.getDepartments();
    }
    if (!this.props.employees.length) {
      this.props.getEmployees();
    }
  }

  changeGoal(goal) {
    this.setState({
      goal,
    });
  }

  toggleModal(goal) {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
      goal: typeof goal === 'string' ? goal : this.state.goal,
    });
  }

  toggleCreateModal() {
    console.log('this.state.goal', this.state.goal);
    this.changeCurrent(null);
    this.toggleModal('create');
  }

  changeCurrent(currentEmployee) {
    this.setState({
      currentEmployee,
    });
  }

  render() {
    /*eslint no-shadow: 0*/
    const {
      employees,
      addEmployee,
      updateEmployee,
      removeEmployee,
      departments,
    } = this.props;

    if (!employees.length || !departments.length) {
      return (
        <h1>Loading Employees...</h1>
      );
    }

    const getDepartmentName = (id) => departments.find(dep => dep.id === id).name;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Employees list
                <Button style={{ marginTop: 0 }} className="float-right" color="success" onClick={this.toggleCreateModal} >Create a new employee</Button>
              </CardHeader>
              <CardBlock className="card-body">
                <Table responsive>
                  <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Department</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      employees.map(({
                        id,
                        firstName,
                        lastName,
                        departmentId,
                      }) =>
                        (<Employee
                          key={id}
                          id={id}
                          firstName={firstName}
                          lastName={lastName}
                          departmentId={departmentId}
                          departmentName={getDepartmentName(departmentId)}
                          goal={this.state.goal}
                          onChangeGoal={this.changeGoal}
                          onToggleModal={this.toggleModal}
                          onChangeCurrent={this.changeCurrent}
                          onRemoveEmployee={removeEmployee}
                        />)
                      )
                    }

                  </tbody>
                </Table>
                <PutEmployee
                  onAddEmployee={addEmployee}
                  onUpdateEmployee={updateEmployee}
                  onToggleModal={this.toggleModal}
                  isOpenModal={this.state.isOpenModal}
                  toggleModal={this.toggleModal}
                  goal={this.state.goal}
                  initData={this.state.currentEmployee}
                />
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

Employees.propTypes = {
  employees: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    departmentId: PropTypes.number.isRequired,
  })).isRequired,
  departments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  getEmployees: PropTypes.func.isRequired,
  addEmployee: PropTypes.func.isRequired,
  updateEmployee: PropTypes.func.isRequired,
  removeEmployee: PropTypes.func.isRequired,
  getDepartments: PropTypes.func.isRequired,
};

Employees.defaultProps = {
  employees: [],
};

const mapStateToProps = state => ({
  employees: state.employees,
  departments: state.departments,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({
    getEmployees,
    addEmployee,
    updateEmployee,
    removeEmployee,
    getDepartments,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Employees);

