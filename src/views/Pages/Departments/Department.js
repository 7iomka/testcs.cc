import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
  Col,
  Card, CardBlock, CardHeader, CardFooter,
  Alert, Button,
  Input, InputGroup, InputGroupAddon,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class Department extends PureComponent {
  constructor(props) {
    super(props);
    this.departmentName = props.name;
    this.state = {
      counter: 0,
      // state for alert when department name changed
      alert: {
        visible: false,
        timeToClose: 4000,
        timeoutId: 0,
      },
      // state for danger modal before we delete the department
      danger: false,
      // state for update-button
      edit: false,
    };

    this.onDismiss = this.onDismiss.bind(this);
    this.onChangeDepartmentName = this.onChangeDepartmentName.bind(this);
    this.updateDepartment = this.updateDepartment.bind(this);
    this.removeDepartment = this.removeDepartment.bind(this);
    this.setDepartmentName = this.setDepartmentName.bind(this);
    this.toggleDanger = this.toggleDanger.bind(this);
  }

  componentDidUpdate(previousProps) {
    console.log('!= ', previousProps.name !== this.departmentName);
    console.log('dep state', this.state);
  }

  onChangeDepartmentName(e) {
    if (this.departmentName !== e.currentTarget.value) {
      return this.setState({
        edit: true,
      });
    }
    return this.setState({
      edit: false,
    });
  }

  onDismiss() {
    this.setState({
      alert: {
        ...this.state.alert,
        visible: false,
        timeoutId: 0,
      },
    });
  }

  setDepartmentName(e) {
    this.departmentName = e.target.value;
  }

  toggleDanger() {
    this.setState({
      danger: !this.state.danger,
    });
  }

  updateDepartment() {
    const { id, name } = this.props;
    const departmentName = this.departmentName;

    const {
      timeoutId: prevTimeoutId,
      timeToClose,
    } = this.state.alert;

    if (prevTimeoutId) {
      clearTimeout(prevTimeoutId);
    }

    const timeoutId = setTimeout(this.onDismiss, timeToClose);
    setTimeout(() => {
      this.setState({
        alert: {
          ...this.state.alert,
          visible: true,
          timeoutId,
        },
        edit: false,
      });
    }, 10);

    if (departmentName && departmentName !== name) {
      this.props.onUpdateDepartment({ id, name: departmentName });
    }
  }

  removeDepartment() {
    const { id } = this.props;
    this.props.onRemoveDepartment({ id });
  }

  render() {
    const { id } = this.props;
    return (
      <Col md="3">
        <Card>
          <CardHeader>
            <span className="float-right badge badge-success badge-pill">{id}</span>
          </CardHeader>
          <CardBlock className="card-body p-4">
            <InputGroup >
              <InputGroupAddon><i className="icon-note" /></InputGroupAddon>
              <Input
                type="text"
                placeholder="Department Name"
                defaultValue={this.departmentName}
                onBlur={this.setDepartmentName}
                onChange={this.onChangeDepartmentName}
              />
            </InputGroup>
          </CardBlock>
          <CardFooter className="py-2 px-4 card-footer text-right">
            <Button onClick={this.updateDepartment} className="mr-2" color="success" disabled={!this.state.edit}>Update Department</Button>
            <Button color="danger" onClick={this.toggleDanger} ><i className="icon-trash" /></Button>
            <Alert className="mt-2" color="success" isOpen={this.state.alert.visible} toggle={this.onDismiss}>
              Department was successfully renamed into &quot;{this.departmentName}&quot;
            </Alert>
            {
              this.state.danger && (
                <Modal
                  isOpen={this.state.danger}
                  toggle={this.toggleDanger}
                  className="modal-danger"
                >
                  <ModalHeader toggle={this.toggleDanger}>Are you sure?</ModalHeader>
                  <ModalBody>
                    Are you sure you want to delete the department?
                  </ModalBody>
                  <ModalFooter>
                    <Button color="danger" onClick={this.removeDepartment}>Yes, delete!</Button>{' '}
                    <Button color="secondary" onClick={this.toggleDanger}>Cancel</Button>
                  </ModalFooter>
                </Modal>
              )
            }
          </CardFooter>
        </Card>
      </Col>
    );
  }
}

Department.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onUpdateDepartment: PropTypes.func.isRequired,
  onRemoveDepartment: PropTypes.func.isRequired,
};

Department.defaultProps = {

};

export default Department;
