import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  Row, Col,
  Card, CardBlock, CardHeader,
} from 'reactstrap';

import {
  getDepartments,
  addDepartment,
  updateDepartment,
  removeDepartment,
} from '../../../redux/actions/departments';

import Department from './Department';
import NewDepartment from './NewDepartment';

class Departments extends Component {
  componentWillMount() {
    if (!this.props.departments.length) {
      this.props.getDepartments();
    }
  }

  render() {
    /*eslint no-shadow: 0*/
    const {
      departments,
      addDepartment,
      updateDepartment,
      removeDepartment,
    } = this.props;

    if (!departments.length) {
      return (
        <h1>Loading Departments...</h1>
      );
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12">
            <Card >
              <CardHeader>
                <i className="fa fa-align-justify" /> Departments list
                <NewDepartment onAddDepartment={addDepartment} />
              </CardHeader>
              <CardBlock>
                <Row >
                  {
                    departments.map(({ id, name }) =>
                      (<Department
                        key={id}
                        id={id}
                        name={name}
                        onUpdateDepartment={updateDepartment}
                        onRemoveDepartment={removeDepartment}
                      />)
                    )
                  }
                </Row>
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
Departments.propTypes = {
  departments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  getDepartments: PropTypes.func.isRequired,
  addDepartment: PropTypes.func.isRequired,
  updateDepartment: PropTypes.func.isRequired,
  removeDepartment: PropTypes.func.isRequired,
};

Departments.defaultProps = {
  departments: [],
};
const mapStateToProps = state => ({ departments: state.departments });

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    getDepartments,
    addDepartment,
    updateDepartment,
    removeDepartment,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Departments);
