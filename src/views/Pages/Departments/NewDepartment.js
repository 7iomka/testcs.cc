import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Input, InputGroup, InputGroupAddon,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class NewDepartment extends Component {
  constructor(props) {
    super(props);
    this.cacheName = '';
    this.state = {
      add: false,
    };
    this.toggleAdd = this.toggleAdd.bind(this);
    this.addDepartment = this.addDepartment.bind(this);
    this.cacheNewDepartmentName = this.cacheNewDepartmentName.bind(this);
  }
  addDepartment() {
    const name = this.cacheName;
    this.props.onAddDepartment({ name });
    this.toggleAdd();
  }
  cacheNewDepartmentName(e) {
    this.cacheName = e.target.value;
  }

  toggleAdd() {
    this.setState({
      add: !this.state.add,
    });
  }

  render() {
    return (
      <div className="float-right" >
        <Button style={{ marginTop: 0 }} onClick={this.toggleAdd} color="success">New department</Button>
        <Modal
          className="modal-success"
          isOpen={this.state.add}
          toggle={this.toggleAdd}
        >
          <ModalHeader toggle={this.toggleAdd}>Create a new department</ModalHeader>
          <ModalBody>
            <InputGroup >
              <InputGroupAddon><i className="icon-note" /></InputGroupAddon>
              <Input
                type="text"
                placeholder="Department Name"
                onBlur={this.cacheNewDepartmentName}
              />
            </InputGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleAdd}>Cancel</Button>
            <Button color="success" onClick={this.addDepartment}>Add</Button>{' '}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

NewDepartment.propTypes = {
  onAddDepartment: PropTypes.func.isRequired,
};

export default NewDepartment;
