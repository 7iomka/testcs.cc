const routes = {
  '/': 'Home',
  '/dashboard': 'Dashboard',
  '/departments': 'Departments',
  '/employees': 'Employees',
};
export default routes;
