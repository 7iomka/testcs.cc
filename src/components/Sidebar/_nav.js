export default {
  items: [
    {
      divider: true,
    },
    {
      title: true,
      name: 'Main',
    },
    {
      name: 'Pages',
      url: '/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'Departments',
          url: '/departments',
          icon: 'fa fa-building',
        },
        {
          name: 'Employees',
          url: '/employees',
          icon: 'icon-people',
        },
      ],
    },
  ],
};
