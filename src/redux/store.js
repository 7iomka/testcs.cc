import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import getReduxDevTools from './utils/getReduxDevTools';
import rootReducer from './reducers';
import rootSaga from './sagas/sagas';


export default function configureStore(initialState) {
  // create the saga middleware
  const sagaMiddleware = createSagaMiddleware();
  // mount it on the Store middleware list
  const middlewares = [
    sagaMiddleware,
  ];

  const store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middlewares), getReduxDevTools())
  );

  // Enable Webpack hot module replacement for reducers
  // this will be cut out in production
  // if (module.hot) {
  //   module.hot.accept('reducers', () => {
  //     const nextRootReducer = require('./reducers');
  //
  //     store.replaceReducer(nextRootReducer);
  //   });
  // }

  // Register all sagas
  rootSaga.map(saga => sagaMiddleware.run(saga));

  return store;
}
