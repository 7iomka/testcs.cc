export const EMPLOYEE_GET = 'EMPLOYEES/GET';
export const EMPLOYEE_GET_SUCCESS = 'EMPLOYEES/GET/SUCCESS';
export const EMPLOYEE_GET_FAILED = 'EMPLOYEES/GET/FAILED';

export const EMPLOYEE_ADD = 'EMPLOYEES/ADD';
export const EMPLOYEE_ADD_SUCCESS = 'EMPLOYEES/ADD/SUCCESS';
export const EMPLOYEE_ADD_FAILED = 'EMPLOYEES/ADD/FAILED';
export const NEW_EMPLOYEE_FORM_NAME = 'NewEmployeeForm';

export const EMPLOYEE_UPDATE = 'EMPLOYEES/UPDATE';
export const EMPLOYEE_UPDATE_SUCCESS = 'EMPLOYEES/UPDATE/SUCCESS';
export const EMPLOYEE_UPDATE_FAILED = 'EMPLOYEES/UPDATE/FAILED';

export const EMPLOYEE_REMOVE = 'EMPLOYEES/REMOVE';
export const EMPLOYEE_REMOVE_SUCCESS = 'EMPLOYEES/REMOVE/SUCCESS';
export const EMPLOYEE_REMOVE_FAILED = 'EMPLOYEES/REMOVE/FAILED';

export const getEmployees = () => ({ type: EMPLOYEE_GET });
export const getEmployeesSuccess = data => ({ type: EMPLOYEE_GET_SUCCESS, data });
export const getEmployeesFailed = error => ({ type: EMPLOYEE_GET_FAILED, error });

export const addEmployee = (data) => ({ type: EMPLOYEE_ADD, data });
export const addEmployeeSuccess = (data) => ({ type: EMPLOYEE_ADD_SUCCESS, data });
export const addEmployeeFailed = (error) => ({ type: EMPLOYEE_ADD_FAILED, error });

export const updateEmployee = (data) => ({ type: EMPLOYEE_UPDATE, data });
export const updateEmployeeSuccess = (data) => ({ type: EMPLOYEE_UPDATE_SUCCESS, data });
export const updateEmployeeFailed = (error) => ({ type: EMPLOYEE_UPDATE_FAILED, error });

export const removeEmployee = (data) => ({ type: EMPLOYEE_REMOVE, data });
export const removeEmployeeSuccess = (data) => ({ type: EMPLOYEE_REMOVE_SUCCESS, data });
export const removeEmployeeFailed = (error) => ({ type: EMPLOYEE_REMOVE_FAILED, error });
