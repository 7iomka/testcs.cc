export const DEPARTMENT_GET = 'DEPARTMENTS/GET';
export const DEPARTMENT_GET_SUCCESS = 'DEPARTMENTS/GET/SUCCESS';
export const DEPARTMENT_GET_FAILED = 'DEPARTMENTS/GET/FAILED';

export const DEPARTMENT_ADD = 'DEPARTMENTS/ADD';
export const DEPARTMENT_ADD_SUCCESS = 'DEPARTMENTS/ADD/SUCCESS';
export const DEPARTMENT_ADD_FAILED = 'DEPARTMENTS/ADD/FAILED';

export const DEPARTMENT_UPDATE = 'DEPARTMENTS/UPDATE';
export const DEPARTMENT_UPDATE_SUCCESS = 'DEPARTMENTS/UPDATE/SUCCESS';
export const DEPARTMENT_UPDATE_FAILED = 'DEPARTMENTS/UPDATE/FAILED';

export const DEPARTMENT_REMOVE = 'DEPARTMENTS/REMOVE';
export const DEPARTMENT_REMOVE_SUCCESS = 'DEPARTMENTS/REMOVE/SUCCESS';
export const DEPARTMENT_REMOVE_FAILED = 'DEPARTMENTS/REMOVE/FAILED';

export const getDepartments = () => ({ type: DEPARTMENT_GET });
export const getDepartmentsSuccess = data => ({ type: DEPARTMENT_GET_SUCCESS, data });
export const getDepartmentsFailed = error => ({ type: DEPARTMENT_GET_FAILED, error });

export const addDepartment = (data) => ({ type: DEPARTMENT_ADD, data });
export const addDepartmentSuccess = (data) => ({ type: DEPARTMENT_ADD_SUCCESS, data });
export const addDepartmentFailed = (error) => ({ type: DEPARTMENT_ADD_FAILED, error });

export const updateDepartment = (data) => ({ type: DEPARTMENT_UPDATE, data });
export const updateDepartmentSuccess = (data) => ({ type: DEPARTMENT_UPDATE_SUCCESS, data });
export const updateDepartmentFailed = (error) => ({ type: DEPARTMENT_UPDATE_FAILED, error });

export const removeDepartment = (data) => ({ type: DEPARTMENT_REMOVE, data });
export const removeDepartmentSuccess = (data) => ({ type: DEPARTMENT_REMOVE_SUCCESS, data });
export const removeDepartmentFailed = (error) => ({ type: DEPARTMENT_REMOVE_FAILED, error });
