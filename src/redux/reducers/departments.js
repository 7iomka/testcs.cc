// import { findIndex } from 'lodash';
import {
  DEPARTMENT_GET_FAILED, DEPARTMENT_GET_SUCCESS,
  DEPARTMENT_ADD_SUCCESS, DEPARTMENT_ADD_FAILED,
  DEPARTMENT_UPDATE_SUCCESS, DEPARTMENT_UPDATE_FAILED,
  DEPARTMENT_REMOVE_SUCCESS, DEPARTMENT_REMOVE_FAILED,
} from '../actions/departments';


export default (state = [], action) => {
  const { type, data, error } = action;

  switch (type) {
    case DEPARTMENT_GET_SUCCESS:
      return data;

    case DEPARTMENT_GET_FAILED:
      return error;

    case DEPARTMENT_ADD_SUCCESS: {
      // post request to API return id an name
      // of new added department
      const { id, name } = data;
      return [
        ...state,
        { id, name },
      ];
    }

    case DEPARTMENT_ADD_FAILED:
      return error;

    case DEPARTMENT_UPDATE_SUCCESS: {
      const { id, name } = data;
      const newState = [...state];
      const index = newState.findIndex(dep => dep.id === Number(id));
      newState[index].name = name;
      return newState;
    }

    case DEPARTMENT_UPDATE_FAILED:
      return error;

    case DEPARTMENT_REMOVE_SUCCESS: {
      const { id } = data;
      return [...state].filter(dep => dep.id !== id);
    }

    case DEPARTMENT_REMOVE_FAILED:
      return error;

    default:
      return state;
  }
};
