import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import departments from './departments';
import employees from './employees';

export default combineReducers({
  departments,
  employees,
  form: formReducer,
});
