import {
  EMPLOYEE_GET_FAILED, EMPLOYEE_GET_SUCCESS,
  EMPLOYEE_ADD_SUCCESS, EMPLOYEE_ADD_FAILED,
  EMPLOYEE_UPDATE_SUCCESS, EMPLOYEE_UPDATE_FAILED,
  EMPLOYEE_REMOVE_SUCCESS, EMPLOYEE_REMOVE_FAILED,
} from '../actions/employees';

export default (state = [], action) => {
  const { type, data, error } = action;

  switch (type) {
    case EMPLOYEE_GET_SUCCESS:
      return data;

    case EMPLOYEE_GET_FAILED:
      return error;

    case EMPLOYEE_ADD_SUCCESS: {
      // post request to API return id an name
      // of new added department

      return [
        ...state,
        { ...data },
      ];
    }

    case EMPLOYEE_ADD_FAILED:
      return error;

    case EMPLOYEE_UPDATE_SUCCESS: {
      // First variant -- need sorting
      // const filteredState = [...state].filter(emp => emp.id !== Number(data.id));
      // return [
      //   ...filteredState,
      //   data,
      // ].sort((e1, e2) => (e1.id > e2.id) ? 1 : (e1.id < e2.id) ? -1 : 0);

      // second variant
      const newState = [...state];
      const index = newState.findIndex(emp => emp.id === Number(data.id));
      newState[index] = {
        ...data,
      };
      return newState;
    }

    case EMPLOYEE_UPDATE_FAILED:
      return error;

    case EMPLOYEE_REMOVE_SUCCESS: {
      const { id } = data;
      return [...state].filter(emp => emp.id !== Number(id));
    }

    case EMPLOYEE_REMOVE_FAILED:
      return error;

    default:
      return state;
  }
};
