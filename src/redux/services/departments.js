import axios from 'axios';

import config from 'constants/config';

const URI = config.apiUrl;

export function getDepartmentsApi() {
  // Optionally the request above could also be done as
  return axios.get(`${URI}/departments`, {
    params: {},
  })
    .then(response => ({ response }))
    .catch(error => ({ error }));
}

export function addDepartmentApi([name]) {
  // Optionally the request above could also be done as
  return axios.post(`${URI}/departments`, {
    name,
  })
    .then(response => ({ response }))
    .catch(error => ({ error }));
}

export function updateDepartmentApi([id, name]) {
  // Optionally the request above could also be done as
  return axios.put(
    `${URI}/departments/${id}`,
    {
      name,
    },
  )
    .then(response => ({ response }))
    .catch(error => ({ error: error.response }));
}

export function removeDepartmentApi([id]) {
  // Optionally the request above could also be done as
  return axios.delete(
    `${URI}/departments/${id}`,
  )
    .then(response => ({ response }))
    .catch(error => ({ error: error.response }));
}
