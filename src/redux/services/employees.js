import axios from 'axios';

import config from 'constants/config';

const URI = config.apiUrl;

export function getEmployeesApi() {
  // Optionally the request above could also be done as
  return axios.get(`${URI}/employees`, {
    params: {},
  })
    .then(response => ({ response }))
    .catch(error => ({ error }));
}

export function addEmployeeApi([{ id, ...rest }]) {
  // Optionally the request above could also be done as
  return axios.post(`${URI}/employees`, {
    ...rest,
  })
    .then(response => ({ response }))
    .catch(error => ({ error }));
}

export function updateEmployeeApi([{ id, ...rest }]) {
  // Optionally the request above could also be done as
  return axios.put(
    `${URI}/employees/${id}`,
    {
      ...rest,
    },
  )
    .then(response => ({ response }))
    .catch(error => ({ error: error.response }));
}

export function removeEmployeeApi([id]) {
  // Optionally the request above could also be done as
  return axios.delete(
    `${URI}/employees/${id}`,
  )
    .then(response => ({ response }))
    .catch(error => ({ error: error.response }));
}
