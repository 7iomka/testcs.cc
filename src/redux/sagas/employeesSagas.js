import { put, all, takeEvery } from 'redux-saga/effects';
import safeCall from './safeCall';
// API Methods
import {
  getEmployeesApi,
  addEmployeeApi,
  updateEmployeeApi,
  removeEmployeeApi,
} from '../services/employees';

// Actions and it constants
import {
  EMPLOYEE_GET,
  EMPLOYEE_ADD,
  EMPLOYEE_UPDATE,
  EMPLOYEE_REMOVE,
  getEmployeesSuccess,
  getEmployeesFailed,
  addEmployeeSuccess,
  addEmployeeFailed,
  updateEmployeeSuccess,
  updateEmployeeFailed,
  removeEmployeeSuccess,
  removeEmployeeFailed,
} from '../actions/employees';


// 1. START SAGAS GENERATORS

/**
 * Get all employees
 */
export function* getEmployees() {
  const { response, error } = yield safeCall(getEmployeesApi);
  if (response) {
    yield put(getEmployeesSuccess(response.data));
  } else {
    yield put(getEmployeesFailed(error));
  }
}

/**
 * Add employee
 * @param action
 */
export function* addEmployee(action) {
  const { response, error } = yield safeCall(addEmployeeApi, [{ ...action.data }]);
  if (response) {
    console.log(response);
    yield put(addEmployeeSuccess(response.data));
  } else {
    console.log(error);
    yield put(addEmployeeFailed(error));
  }
}

/**
 * Update single employee
 * @param action
 */
export function* updateEmployee(action) {
  const { response, error } = yield safeCall(updateEmployeeApi, [action.data]);
  if (response) {
    console.log(response);
    yield put(updateEmployeeSuccess(response.data));
  } else {
    console.log(error);
    yield put(updateEmployeeFailed(error));
  }
}

/**
 * Remove a single employee
 * @param action
 */
export function* removeEmployee(action) {
  const { id } = action.data;
  const { response, error } = yield safeCall(removeEmployeeApi, [id]);
  if (response) {
    console.log(response);
    yield put(removeEmployeeSuccess({ id }));
  } else {
    console.log(error);
    yield put(removeEmployeeFailed(error));
  }
}

// 2. START SAGA WATCHERS
// They spawn a new task on each action

export function* watchGetEmployees() {
  yield takeEvery(EMPLOYEE_GET, getEmployees);
}

export function* watchAddEmployee() {
  yield takeEvery(EMPLOYEE_ADD, addEmployee);
}

export function* watchUpdateEmployee() {
  yield takeEvery(EMPLOYEE_UPDATE, updateEmployee);
}

export function* watchRemoveEmployee() {
  yield takeEvery(EMPLOYEE_REMOVE, removeEmployee);
}

// 3. ROOT SAGA
// This is single entry point to start sagas at one
export default function* rootSaga() {
  yield all([
    watchGetEmployees(),
    watchAddEmployee(),
    watchUpdateEmployee(),
    watchRemoveEmployee(),
  ]);
}
