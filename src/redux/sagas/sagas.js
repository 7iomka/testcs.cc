import departmentsSagas from './departmentsSagas';
import employeesSagas from './employeesSagas';

export default [
  departmentsSagas,
  employeesSagas,
];
