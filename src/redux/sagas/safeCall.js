import { call } from 'redux-saga/effects';
// Safe call for prevent stopping all sagas
export default function safeCall(...args) {
  return call(function* safeWrapper() {
    try {
      return yield call(...args);
    } catch (e) {
      return null;
    }
  });
}
