import { put, all, takeEvery } from 'redux-saga/effects';
import safeCall from './safeCall';
// API Methods
import {
  getDepartmentsApi,
  addDepartmentApi,
  updateDepartmentApi,
  removeDepartmentApi,
} from '../services/departments';

// Actions and it constants
import {
  DEPARTMENT_GET,
  DEPARTMENT_ADD,
  DEPARTMENT_UPDATE,
  DEPARTMENT_REMOVE,
  getDepartmentsSuccess,
  getDepartmentsFailed,
  addDepartmentSuccess,
  addDepartmentFailed,
  updateDepartmentSuccess,
  updateDepartmentFailed,
  removeDepartmentSuccess,
  removeDepartmentFailed,
} from '../actions/departments';


// 1. START SAGAS GENERATORS

/**
 * Get all departments
 */
export function* getDepartments() {
  const { response, error } = yield safeCall(getDepartmentsApi);
  if (response) {
    yield put(getDepartmentsSuccess(response.data));
  } else {
    yield put(getDepartmentsFailed(error));
  }
}

/**
 * Add department
 * @param action
 */
export function* addDepartment(action) {
  const { name } = action.data;
  const { response, error } = yield safeCall(addDepartmentApi, [name]);
  if (response) {
    console.log(response);
    yield put(addDepartmentSuccess(response.data));
  } else {
    console.log(error);
    yield put(addDepartmentFailed(error));
  }
}

/**
 * Update single department
 * @param action
 */
export function* updateDepartment(action) {
  const { id, name } = action.data;
  const { response, error } = yield safeCall(updateDepartmentApi, [id, name]);
  if (response) {
    console.log(response);
    yield put(updateDepartmentSuccess(response.data));
  } else {
    console.log(error);
    yield put(updateDepartmentFailed(error));
  }
}

/**
 * Remove a single department
 * @param action
 */
export function* removeDepartment(action) {
  const { id } = action.data;
  const { response, error } = yield safeCall(removeDepartmentApi, [id]);
  if (response) {
    console.log('removeDep response', response);
    yield put(removeDepartmentSuccess({ id }));
  } else {
    console.log(error);
    yield put(removeDepartmentFailed(error));
  }
}

// 2. START SAGA WATCHERS
// They spawn a new task on each action

export function* watchGetDepartments() {
  yield takeEvery(DEPARTMENT_GET, getDepartments);
}

export function* watchAddDepartment() {
  yield takeEvery(DEPARTMENT_ADD, addDepartment);
}

export function* watchUpdateDepartment() {
  yield takeEvery(DEPARTMENT_UPDATE, updateDepartment);
}

export function* watchRemoveDepartment() {
  yield takeEvery(DEPARTMENT_REMOVE, removeDepartment);
}

// 3. ROOT SAGA
// This is single entry point to start sagas at one
export default function* rootSaga() {
  yield all([
    watchGetDepartments(),
    watchAddDepartment(),
    watchUpdateDepartment(),
    watchRemoveDepartment(),
  ]);
}
