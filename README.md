This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Table of Contents

- [Available Scripts](#available-scripts)
  - [npm start](#npm-start)


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

This command parallel starts the json-server that listens to the port 3030
This module makes an mini-api located at the address [http://localhost:3030](http://localhost:3030)
Api models available:
  Departments: [http://localhost:3030/departments](http://localhost:3030/departments)
  Employees: [http://localhost:3030/employees](http://localhost:3030/employees)
